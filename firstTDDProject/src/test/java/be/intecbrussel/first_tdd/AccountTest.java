package be.intecbrussel.first_tdd;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class AccountTest {

    @Test
    public void balanceOfNewAccountIsZero(){
        Account account = new Account();
        assertEquals(BigDecimal.ZERO, account.getBalance());
    }

    @Test
    public void balanceAfterFirstDepositEqualsFirstDeposit(){
        Account account = new Account();
        BigDecimal balance = new BigDecimal(5000);
        account.deposit(balance);
        assertEquals(balance, account.getBalance());
    }

    @Test
    public void balanceAfterSecondDepositIsSumOfDeposits(){
        Account account = new Account();
        account.deposit(new BigDecimal(5000));
        account.deposit(new BigDecimal(10000));
        assertEquals(0, new BigDecimal(15000).compareTo(account.getBalance()));

    }
}
