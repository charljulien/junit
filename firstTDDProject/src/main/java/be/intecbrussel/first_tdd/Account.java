package be.intecbrussel.first_tdd;

import java.math.BigDecimal;

public class Account {

    private BigDecimal balance;

    public Account() {
        balance = new BigDecimal(0);
    }

    public BigDecimal deposit(BigDecimal amount){
        balance = balance.add(amount);
        return balance;
    }

    public BigDecimal getBalance(){
        return balance;
    }
}
