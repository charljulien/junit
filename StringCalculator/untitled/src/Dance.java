public class Dance {

    public static void main(String[] args) {
        int steps = 6;
        System.out.println(countStep(steps));
    }

    private static int countStep(int numberMove) {
        int last_move = -2;
        int penultimate_move = 1;
        int previous_position = -1;
        int current_position = 0;
        for(int i = 0;i<numberMove;i++){
            //compute current move and position
            int current_move = last_move - penultimate_move;
            current_position = previous_position + current_move;

            //do switch in here
            penultimate_move = last_move;
            last_move = current_move;
            previous_position = current_position;
        }
        return current_position;
    }
}
