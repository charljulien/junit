import com.sun.jmx.snmp.internal.SnmpSubSystem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringCalculatorTest {
    StringCalculator stringCalculator = new StringCalculator();

    @Test
    public void testStringIsNull() throws Exception {
        assertEquals(0, stringCalculator.add(""));
    }

    @Test
    public void testStringIsOne() throws Exception {
        assertEquals(1, stringCalculator.add("1"));
    }

    @Test
    public void testStringIsTwo() throws Exception {
        assertEquals(3, stringCalculator.add("1,2"));
    }

    @Test
    public void testStringIsWhatever() throws Exception {
        assertEquals(65, stringCalculator.add("1,2,50,12"));
    }

    @Test
    public void testStringIsWithMultiSeparators() throws Exception {
        assertEquals(65, stringCalculator.add(";\n1;2;50;12"));
    }

    @Test
    public void testStringWithPersonifySeparator() throws Exception {
        assertEquals(3,stringCalculator.add(";\n1;2"));
    }

    @Test
    public void testStringWithPersonifySeparatorAndCheckNegativeNumbers() throws Exception {
        assertEquals(6,stringCalculator.add(";\n1;5;0;0"));
    }

    @Test
    public void testStringWithNumberAboveOneThousantShouldBeIgnored() throws Exception{
        assertEquals(6,stringCalculator.add(";\n1;5;0;1001"));
    }

    @Test
    public void testStringWithDelimiterAnylength() throws Exception{
        assertEquals(6,stringCalculator.add("\\*\\*\\*\n1***5***0***1001"));
    }

    /**
    Werkt niet met speciale caracters zoals \ of |
    **/
    @Test
    public void testStringWithMultipleDelimiter() throws Exception{
        assertEquals(6,stringCalculator.add("%$/\n1*5-0/1001"));
    }
}
