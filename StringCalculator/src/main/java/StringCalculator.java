import java.util.ArrayList;
import java.util.List;

public class StringCalculator {
    private String delimiter;
    private String numberToIterate;
    private boolean negativeNumber = false;
    private List<Integer>negativeNumbers= new ArrayList<Integer>();
    private List <Character>delimiters = new ArrayList<Character>();

    public StringCalculator() {
    }

    public int add(String numbers) throws Exception {
        int numberInt = 0;
        int newNumber;
        String multripleDelimiter;
        if (numbers == ""){
        }else{
            delimiter = getDelimiter(numbers);
            numberToIterate = getNumberToIterate();
            multripleDelimiter = getMultipleDelimiter(delimiter);
            System.out.println("multipleDelimiter = "+multripleDelimiter);
            System.out.println("numberToIterate = " +numberToIterate);
            String[] numberArray = numberToIterate.split(delimiter);
            System.out.println("numberArray = " +numberArray);
            for (String numberString : numberArray){
                try {
                    newNumber = Integer.parseInt(numberString);
                    checkIfNegative(newNumber);
                    newNumber = checkIfAboveOneThouse(newNumber);
                    numberInt += newNumber;
                }catch (NumberFormatException ex){
                    System.out.println("Input number !" + numberString);
                }
            }
        }
        if(negativeNumber){
            throw new Exception("These numbers are negative"+String.valueOf(negativeNumbers));
        }
        return numberInt;
    }

    private String getMultipleDelimiter(String delimiter) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i< delimiter.length();i++){
            sb.append(delimiter.charAt(i)).append("|");
        }
        sb.deleteCharAt(delimiter.length()+1);
        System.out.println("stringbuilder = "+sb);
        return sb.toString();
    }

    private int checkIfAboveOneThouse(int newNumber) {
        if(newNumber > 1000){
            newNumber = 0;
        }
        return newNumber;
    }

    private void checkIfNegative(int newNumber) {
        if (newNumber < 0){
            setNegativeNumber(true);
            negativeNumbers.add(newNumber);
        }
    }

    public String getDelimiter(String numbers) {
        String delimiter = "";
        for (int i = 0; i < numbers.length(); i++){
            if (numbers.charAt(i)=='\n'){
                System.out.println("index = "+i+" "+numbers.charAt(i));
                delimiter = numbers.substring(0,i);
                setNumberToIterate(numbers.substring(i+1));
                System.out.println("delimiter dans la boucle= "+delimiter);
                System.out.println("numberArray dans la boucle= "+ numberToIterate);
                break;
            }else{

            }
        }
        System.out.println("delimiter en sortie de boucle= "+delimiter);
        return delimiter;
    }

    public void setNumberToIterate(String numberToIterate) {
        this.numberToIterate = numberToIterate;
    }
    public String getNumberToIterate() {
        return numberToIterate;
    }
    public List<Integer> getNegativeNumbers() {
        return negativeNumbers;
    }
    public boolean isNegativeNumber() {
        return negativeNumber;
    }
    public void setNegativeNumber(boolean negativeNumber) {
        this.negativeNumber = negativeNumber;
    }
    public List<Character> getDelimiters() {
        return delimiters;
    }
}
